call plug#begin('~/.local/share/nvim/plugged')
Plug 'ntpeters/vim-better-whitespace'
Plug 'klen/python-mode'
Plug 'stephpy/vim-yaml'
Plug 'kristijanhusak/vim-hybrid-material'
" markdown start
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
" markdown end
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'
Plug 'fatih/vim-go'
Plug 'gregsexton/gitv'
Plug 'jreybert/vimagit'
Plug 'pearofducks/ansible-vim'
Plug 'itchyny/lightline.vim'
Plug 'altercation/vim-colors-solarized'
Plug 'KeitaNakamura/neodark.vim'
Plug 'fneu/breezy'
Plug 'matt-deacalion/vim-systemd-syntax'
Plug '/bin/fzf'
Plug 'junegunn/fzf.vim'
Plug 'danro/rename.vim'
Plug 'ap/vim-buftabline'
call plug#end()



let g:lightline = {
      \ 'colorscheme': 'neodark',
      \ }
set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set t_Co=256
set colorcolumn=79
set cursorcolumn
set cursorline

"------------------------------ Material colors START -------------------------
syntax enable
"set termguicolors
"colorscheme breezy
"colorscheme duoduo
"colorscheme subtle_dark
"colorscheme basic-dark
"colorscheme material
"colorscheme material-monokai
"colorscheme molokai
"colorscheme vim-material
colorscheme neodark
"colorscheme deus
let g:enable_italic_font = 1
let g:enable_bold_font = 1
highlight Comment cterm=italic
"------------------------------ Material colors END ---------------------------


"-----------------------Indentation settings START-----------------------------
set tabstop=4
set shiftwidth=4
set expandtab
"-----------------------Indentation settings END-------------------------------


"-----------------------Set list START /trailing symbols/----------------------
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:. "␣
hi WhiteSpaceChar ctermfg=251 guifg=#999999
call matchadd("WhiteSpaceChar", "[ \t]")
"-----------------------Set list END /trailing symbols/----------------------


"----------------------Strip whitespaces with vim-better-whitespace START------
autocmd BufEnter * EnableStripWhitespaceOnSave
"----------------------Strip whitespaces with vim-better-whitespace END--------


"-------------------------Set/Unset line numbers START-------------------------
nmap <C-N><C-N> :set invnumber<CR>
set numberwidth=3
set cpoptions+=n
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE
"-------------------------Set/Unset line numbers END---------------------------


"------------------------------------MARKDOWN START----------------------------
let g:vim_markdown_folding_disabled = 0
set conceallevel=2
let g:vim_markdown_follow_anchor = 1
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
"------------------------------------MARKDOWN END------------------------------


"------------------------------------GIT Gutter START--------------------------
set updatetime=250
"------------------------------------GIT Gutter END----------------------------


"------------------------------------------------------ NETRW START------------
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 20
" Toggle Vexplore with Ctrl-E
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction
map <silent> <C-E> :call ToggleVExplorer()<CR>
" Hit enter in the file browser to open the selected
" file with :vsplit to the right of the browser.
let g:netrw_browse_split = 4
let g:netrw_altv = 1

" Change directory to the current buffer when opening files.
set autochdir
"--------------------------------------------------- NETRW END-----------------


"----------------------- RUST START -------------------------------------------
let g:racer_experimental_completer = 1
"----------------------- RUST END ---------------------------------------------


"----------------------- SYNTASTIC START --------------------------------------
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
"let g:syntastic_python_checkers = ['pylint']
"let g:syntastic_go_checkers = ['gofmt']
"autocmd FileType go autocmd BufWritePre <buffer> Fmt
"----------------------- SYNTASTIC END  ---------------------------------------


"----------------------- GO START ---------------------------------------------
au FileType go set noexpandtab
au FileType go set shiftwidth=4
au FileType go set softtabstop=4
au FileType go set tabstop=4
let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
"----------------------- GO END -----------------------------------------------


"----------------------- ANSIBLE START ----------------------------------------
let g:ansible_unindent_after_newline = 1
let g:ansible_extra_syntaxes = "sh.vim ruby.vim"
let g:ansible_attribute_highlight = "ob"
let g:ansible_name_highlight = 'd'
let g:ansible_extra_keywords_highlight = 1
let g:ansible_normal_keywords_highlight = 'Constant'
"----------------------- ANSIBLE END ------------------------------------------

"------------------ PYTHON MODE START -----------------------------------------
let g:pymode = 1
let g:pymode_python = 'python3'
let g:pymode_rope_completion = 1
let g:pymode_rope_complete_on_dot = 1
let g:pymode_rope_autoimport = 1
let g:pymode_syntax = 1
let g:pymode_syntax_all = 1
let g:pymode_syntax_print_as_function = 0
let g:pymode_syntax_highlight_self = g:pymode_syntax_all
let g:pymode_syntax_indent_errors = g:pymode_syntax_all
let g:pymode_syntax_space_errors = g:pymode_syntax_all
let g:pymode_syntax_string_formatting = g:pymode_syntax_all
let g:pymode_syntax_string_format = g:pymode_syntax_all
let g:pymode_syntax_string_templates = g:pymode_syntax_all
let g:pymode_syntax_doctests = g:pymode_syntax_all
let g:pymode_syntax_builtin_objs = g:pymode_syntax_all
let g:pymode_syntax_builtin_types = g:pymode_syntax_all
let g:pymode_syntax_highlight_exceptions = g:pymode_syntax_all
let g:pymode_syntax_highlight_equal_operator = g:pymode_syntax_all
let g:pymode_syntax_docstrings = g:pymode_syntax_all
let g:pymode_indent = 1
let g:pymode_folding = 0
"------------------ PYTHON MODE END -------------------------------------------


"------------------ FZF MODE START --------------------------------------------
" This is the default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

" Default fzf layout
" - down / up / left / right
let g:fzf_layout = { 'down': '~40%' }

" In Neovim, you can set up fzf window using a Vim command
let g:fzf_layout = { 'window': 'enew' }
let g:fzf_layout = { 'window': '-tabnew' }
let g:fzf_layout = { 'window': '10split enew' }

" [Buffers] Jump to the existing window if possible
let g:fzf_buffers_jump = 1

"------------------ FZF MODE END ----------------------------------------------


"------- Cycle between the buffers with TAB and Shift-Tab START ---------------
set hidden
nnoremap <C-]> :bnext<CR>
nnoremap <C-[> :bprev<CR>
"------- Cycle between the buffers with TAB and Shift-Tab END -----------------
